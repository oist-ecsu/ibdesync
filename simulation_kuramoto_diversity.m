% Code created on 20 December 2023 by Loh Chen Lam, OIST

clear all
close all

[CCORR_1, CCORR_2] = kuramoto_simulation_comparison

% Plotting the results
figure;

subplot(1, 2, 1);
imagesc(CCORR_1);
title('Condition 1: CCORR');
set(gca, 'YDir', 'normal'); % Sets the y-axis direction to normal
set(gca, 'YTick', 1:size(CCORR_1,1)); % Set Y-Ticks from 1 to num_oscillators
colorbar;
caxis([0, 1]); % Set the colorbar scale to [0,1]
axis square;

subplot(1, 2, 2);
imagesc(CCORR_2);
title('Condition 2: CCORR');
set(gca, 'YDir', 'normal'); % Sets the y-axis direction to normal
set(gca, 'YTick', 1:size(CCORR_2,1)); % Set Y-Ticks from 1 to num_oscillators
colorbar;
caxis([0, 1]); % Set the colorbar scale to [0,1]
axis square;

% Calculate and display the values for Condition 1
[mean_intra_brain_1_cond1, mean_inter_brain_cond1, mean_intra_brain_2_cond1] = calculate_mean_ccorr(CCORR_1);
disp(['Condition 1 - Mean Intra-brain CCORR (Brain 1): ', num2str(mean_intra_brain_1_cond1)]);
disp(['Condition 1 - Mean Inter-brain CCORR: ', num2str(mean_inter_brain_cond1)]);
disp(['Condition 1 - Mean Intra-brain CCORR (Brain 2): ', num2str(mean_intra_brain_2_cond1)]);

% Calculate and display the values for Condition 2
[mean_intra_brain_1_cond2, mean_inter_brain_cond2, mean_intra_brain_2_cond2] = calculate_mean_ccorr(CCORR_2);
disp(['Condition 2 - Mean Intra-brain CCORR (Brain 1): ', num2str(mean_intra_brain_1_cond2)]);
disp(['Condition 2 - Mean Inter-brain CCORR: ', num2str(mean_inter_brain_cond2)]);
disp(['Condition 2 - Mean Intra-brain CCORR (Brain 2): ', num2str(mean_intra_brain_2_cond2)]);

function [CCORR_1, CCORR_2] = kuramoto_simulation_comparison
    % Initialize parallel pool
    if isempty(gcp('nocreate'))
        parpool(20);
    end

    % Parameters
    T = 20; % Total time in seconds
    dt = 0.001; % Time step
    num_simulations = 1000; % Number of simulations
    num_oscillators = 20; % Total number of oscillators
    start_calculation_time = 3; % Time after which to start calculations

    % Conditions
    w_ranges = {[39, 41], [30, 50]}; % Frequency ranges for the two conditions

    % Initialize variables to store averaged CCORR matrices
    CCORR_1 = zeros(num_oscillators, num_oscillators);
    CCORR_2 = zeros(num_oscillators, num_oscillators);

    % Run simulations for both conditions
    for condition = 1:2
        w_range = w_ranges{condition};
        rho_sum = zeros(num_oscillators, num_oscillators);

        % Run simulations in parallel
        parfor sim = 1:num_simulations
            disp(sim)
            rho_matrix = run_simulation(T, dt, w_range, num_oscillators, start_calculation_time);
            rho_sum = rho_sum + rho_matrix;
        end

        % Calculate the average CCORR matrix
        if condition == 1
            CCORR_1 = rho_sum / num_simulations;
        else
            CCORR_2 = rho_sum / num_simulations;
        end
    end


end

function rho_matrix = run_simulation(T, dt, w_range, num_oscillators, start_calculation_time)
    N = T/dt;
    start_idx = round(start_calculation_time / dt); % Index to start calculations
    w = rand(num_oscillators, 1) * (w_range(2) - w_range(1)) + w_range(1);
    theta = rand(num_oscillators, 1) * 2 * pi;
    phases = zeros(num_oscillators, N);
    K = 1; % Coupling constant, adjust as needed

    % Define coupling constants
    K_intra = 1; % Coupling within the same brain
    K_inter = 0.5; % Coupling between different brains

    % Simulate the dynamics
    for t = 1:N
        for i = 1:num_oscillators
            sum_sin = 0;
            for j = 1:num_oscillators
                if (i <= 10 && j <= 10) || (i > 10 && j > 10)
                    % Within the same brain
                    sum_sin = sum_sin + K_intra * sin(theta(j) - theta(i));
                else
                    % Between different brains
                    sum_sin = sum_sin + K_inter * sin(theta(j) - theta(i));
                end
            end
            theta(i) = theta(i) + w(i) * dt + sum_sin * dt / num_oscillators;
        end
        phases(:, t) = theta;
    end

    % Compute overall CCORR only for t > start_calculation_time
    rho_matrix = zeros(num_oscillators, num_oscillators);
    for i = 1:num_oscillators
        for j = 1:num_oscillators
            alpha1 = angle(exp(1i * phases(i, start_idx:end)));
            alpha2 = angle(exp(1i * phases(j, start_idx:end)));
            alpha1_bar = angle(mean(exp(1i * alpha1)));
            alpha2_bar = angle(mean(exp(1i * alpha2)));
            sin_diff1 = sin(alpha1 - alpha1_bar);
            sin_diff2 = sin(alpha2 - alpha2_bar);
            num = sum(sin_diff1 .* sin_diff2);
            den = sqrt(sum(sin_diff1.^2) * sum(sin_diff2.^2));
            rho_matrix(i, j) = abs(num / den);
        end
    end
end


function [mean_intra_brain_1, mean_inter_brain, mean_intra_brain_2] = calculate_mean_ccorr(CCORR_matrix)
    % Assuming the matrix is 20x20 and split into two 10x10 brains
    num_oscillators_per_brain = 10;
    
    % Intra-brain CCORR for the first brain (upper left 10x10 submatrix)
    intra_brain_1 = CCORR_matrix(1:num_oscillators_per_brain, 1:num_oscillators_per_brain);
    mean_intra_brain_1 = mean(intra_brain_1(:));
    
    % Intra-brain CCORR for the second brain (lower right 10x10 submatrix)
    intra_brain_2 = CCORR_matrix(num_oscillators_per_brain+1:end, num_oscillators_per_brain+1:end);
    mean_intra_brain_2 = mean(intra_brain_2(:));

    % Inter-brain CCORR (off-diagonal 10x10 submatrices)
    inter_brain_1 = CCORR_matrix(1:num_oscillators_per_brain, num_oscillators_per_brain+1:end);
    inter_brain_2 = CCORR_matrix(num_oscillators_per_brain+1:end, 1:num_oscillators_per_brain);
    mean_inter_brain = mean([inter_brain_1(:); inter_brain_2(:)]);
end
